<?php
$gallery = [
    ['img' => 'assets/img/1.jpg', 'text' => ''],
    ['img' => 'assets/img/2.jpg', 'text' => ''],
    ['img' => 'assets/img/3.jpg', 'text' => ''],
];
?>
<nav data-depth='4' class='breadcrumb hidden-sm-down dor-breadcrumb'>
    <div class='container'>
        <h1 class='category-name'>Fiestas de Té</h1>
    </div>
</nav>
<section class='container'>
    <div id='nanogallery2'>
        <?php foreach ($gallery as $item): ?>
            <a href='<?= $item['img'] ?>' data-ngThumb='<?= $item['img'] ?>'><?= $item['text'] ?></a>
        <?php endforeach; ?>
    </div>
    <h3 style="text-align: center">Quiero una fiesta de té!</h3>
    <div style="text-align: center; margin-top: 30px">
        <button class="btn btn-success" onclick="window.open('https://wa.me/+524776479710','_blank')">
            <i class="fab fa-whatsapp"></i>
            Whatsapp
        </button>
    </div>
</section>
<script>
    $(() => {
        new App.Eventos();
    })
</script>
