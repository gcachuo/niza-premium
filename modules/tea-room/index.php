<section>
    <nav data-depth='4' class='breadcrumb hidden-sm-down dor-breadcrumb'>
        <div class='container'>
            <h1 class='category-name'>Laboratorio de mezclas y bebidas creativas</h1>
        </div>
    </nav>
    <section class="container">
        <div id="category-description" class="text-muted">
            <h2 class="tab">UN SORBO DE ABSOLUTA PLENITUD Y VIDA ETERNA</h2>
            <p>
                ¡VISITA NUESTRO AUTÉNTICO TEA BAR!
                CONTAMOS CON UNA INCREÍBLE Y DELICIOSA VARIEDAD DE BEBIDAS NATURALES Y FRESCAS, HECHAS A BASE DE
                UNA SELECCIÓN DE INFUSIONES DE TÉ PREMIUM INTERNACIONALES, HERBALES, FRUTALES, COLD BREW TEA Y
                TAMBIÉN MIXOLOGÍA CON TÉ.
            </p>

            <h2 class='tab'>INFUSIONES</h2>
            <p>
                LAS INFUSIONES DE TÉ SON HECHAS CON EL PRIMER MÉTODO ANTIGUO/TRADICIONAL DE INFUSIONAR LAS
                "HOJAS
                DE LA SALUD" EN AGUA HIRVIENDO DURANTE DOS A CINCO MINUTOS. SE PUEDE BEBER CALIENTE, FRÍO O
                FROZEN.
            </p>
            <h2 class='tab'>COLD BREW TEA</h2>
            <p>
                ESTE MÉTODO DE INFUSIÓN ES DISTINTO AL TRADICIONAL, ESTE ES UN NUEVO Y MODERNO MÉTODO QUE SE
                HACE INFUSIONANDO EL TÉ DURANTE <span style="font-family: serif">12</span> HORAS EN AGUA HELADA.
            </p>
            <h2 class='tab'>MIXOLOGÍA CON TÉ</h2>
            <p>
                CREAMOS UNA CARTA DE AUTÉNTICAS E INCREÍBLES BEBIDAS PREMIUM HECHAS CON INFUSIONES DE TÉ O COLD
                BREW TEA PRINCIPALMENTE CON MEZCAL, TEQUILA, GINEBRA, WHISKEY, CHAMPÁN Y VINO TINTO O ROSADO.
            </p>
            <center>
                <p>
                    MAPA UBICACIÓN
                </p>
                <iframe
                        width='600'
                        height='450'
                        style='border:0'
                        loading='lazy'
                        allowfullscreen
                        src='https://www.google.com/maps/embed/v1/place?key=AIzaSyB8wE8uJLMQQsh1GXsAEuCbWfyuu_T0LfI
    &q=Leon,Gto'>
                </iframe>
            </center>
        </div>
    </section>
</section>
