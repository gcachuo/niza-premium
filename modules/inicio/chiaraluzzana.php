<!DOCTYPE html><!-- Last Published: Thu Jul 01 2021 08:40:18 GMT+0000 (Coordinated Universal Time) -->
<html data-wf-domain='www.chiaraluzzana.com' data-wf-page='5ed515f3ca38aaac4f683b8c' data-wf-site='5ea69b4027484b2df2b45806' lang='en'>
<!-- Mirrored from www.chiaraluzzana.com/about by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Feb 2022 22:02:53 GMT -->
<head>
    <meta charset="utf-8"/>
    <title>Chiara Luzzana — About</title>
    <meta content="Chiara Luzzana is an award-winning Sound Designer focused on Sound Design, Music Composition, Sound Branding, Soundtrack and Audio Installations collaborating with companies and agencies all over the world."
          name="description"/>
    <meta content="Chiara Luzzana — About" property="og:title"/>
    <meta content="Chiara Luzzana is an award-winning Sound Designer focused on Sound Design, Music Composition, Sound Branding, Soundtrack and Audio Installations collaborating with companies and agencies all over the world."
          property="og:description"/>
    <meta content="https://uploads-ssl.webflow.com/5ea69b4027484b2df2b45806/5ed57135e5322a0e7965eb6c_open-graph.jpg" property="og:image"/>
    <meta content="Chiara Luzzana — About" property="twitter:title"/>
    <meta content="Chiara Luzzana is an award-winning Sound Designer focused on Sound Design, Music Composition, Sound Branding, Soundtrack and Audio Installations collaborating with companies and agencies all over the world."
          property="twitter:description"/>
    <meta content="https://uploads-ssl.webflow.com/5ea69b4027484b2df2b45806/5ed57135e5322a0e7965eb6c_open-graph.jpg" property="twitter:image"/>
    <meta property="og:type" content="website"/>
    <meta content="summary_large_image" name="twitter:card"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <link href="https://uploads-ssl.webflow.com/5ea69b4027484b2df2b45806/css/chiara-luzzana.webflow.de935ea81.min.css" rel="stylesheet" type="text/css"/>
    <style>@media (min-width: 992px) {
            html.w-mod-js:not(.w-mod-ix) [data-w-id="b84a4718-3d32-7794-986d-ff4584ed2164"] {
                -webkit-transform: translate3d(0, 100%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                -moz-transform: translate3d(0, 100%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                -ms-transform: translate3d(0, 100%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                transform: translate3d(0, 100%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
            }

            html.w-mod-js:not(.w-mod-ix) [data-w-id="32eaee09-4cf5-ec96-4bf0-c6da8ea3e53e"] {
                -webkit-transform: translate3d(0, 0%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                -moz-transform: translate3d(0, 0%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                -ms-transform: translate3d(0, 0%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                transform: translate3d(0, 0%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                display: block;
            }

            html.w-mod-js:not(.w-mod-ix) [data-w-id="fade6d6e-aa5a-6018-b8be-75b18fa12882"] {
                -webkit-transform: translate3d(0, 100%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                -moz-transform: translate3d(0, 100%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                -ms-transform: translate3d(0, 100%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
                transform: translate3d(0, 100%, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
            }
        }</style>
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif]-->
    <script type="text/javascript">!function (o, c) {
            var n = c.documentElement, t = " w-mod-";
            n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
        }(window, document);</script>
    <link href="https://uploads-ssl.webflow.com/5ea69b4027484b2df2b45806/5ef5fac683cbb08161c35714_favicon_32.ico" rel="shortcut icon" type="image/x-icon"/>
    <link href="https://uploads-ssl.webflow.com/5ea69b4027484b2df2b45806/5ef5facc1620d59cbcc1b405_favicon_256.ico" rel="apple-touch-icon"/>
    <script type="text/javascript">(function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-170294967-1', 'auto');
        ga('send', 'pageview');</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/simplex-noise/2.4.0/simplex-noise.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/EaselPlugin.min.js'></script>
    <script defer src='https://cdn.jsdelivr.net/npm/locomotive-scroll@4.0.3/dist/locomotive-scroll.min.js'></script>
    <link rel='stylesheet' type='text/css'
          href='https://cdn.jsdelivr.net/gh/locomotivemtl/locomotive-scroll/dist/locomotive-scroll.min.css'/>

    <script src='https://cdn.jsdelivr.net/gh/niccolomiranda/chiara-luzzana/jsSnippets/customCursor10/index.js'></script>

    <script>
        window.isMobile = false
        if (/Mobi|Android/i.test(navigator.userAgent)) {
            window.isMobile = true
        }
        window.isSafari = false
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf('safari') != -1) {
            if (ua.indexOf('chrome') > -1) {
            } else {
                window.isSafari = true
            }
        }

        class SoundToggler {
            constructor() {
                this.bind()

                this.container = document.querySelector('.soundtoggler')
                this.sTCanvas = document.createElement('canvas')
                this.container.appendChild(this.sTCanvas)
                this.ctx = this.sTCanvas.getContext('2d')
                this.sTWidth = this.ctx.canvas.width = this.container.offsetWidth * dpi
                this.sTHeight = this.ctx.canvas.height = this.container.offsetHeight * dpi

                this.ctx.lineWidth = dpi

                this.soundFlag = false
                this.amp = {
                    value: 3
                };
                this.wL = 0.1;
                this.speed = 0.003


                this.container.addEventListener('click', this.click)
                window.addEventListener('resize', this.onResize)
                RAF.subscribe('sineUpdate', this.update)


                this.autoPlay = true
                if (window.isSafari)
                    this.autoPlay = false

                console.log(this.autoPlay, JSON.parse(window.localStorage.getItem('audioWasPlaying')))
                if (this.autoPlay && JSON.parse(window.localStorage.getItem('audioWasPlaying'))) {
                    this.moveFlag = false
                    window.addEventListener('mousemove', this.mouseMove)
                }

            }

            mouseMove() {
                if (!this.autoPlay)
                    return
                if (this.moveFlag)
                    return
                this.moveFlag = true
                console.log(window.localStorage.getItem('audioWasPlaying'))
                this.click()
            }

            update() {
                this.ctx.clearRect(0, 0, this.sTWidth, this.sTHeight)

                this.ctx.beginPath()
                let inc = 0
                while (inc < this.sTWidth) {
                    if (inc == 0)
                        this.ctx.moveTo(inc, this.sTHeight / 2 + this.amp.value * dpi * Math.sin(inc * (1 / (this
                            .container.offsetWidth * 0.01)) * this.wL / dpi + Date.now() * this.speed));
                    else
                        this.ctx.lineTo(inc, this.sTHeight / 2 + this.amp.value * dpi * Math.sin(inc * (1 / (this
                            .container.offsetWidth * 0.01)) * this.wL / dpi + Date.now() * this.speed));
                    inc += 2;
                }
                this.ctx.stroke();
                this.ctx.closePath()

            }

            started() {
                this.soundFlag = this.soundFlag == true ? false : true;
                let target = this.soundFlag == true ? 30 : 3

                TweenLite.to(this.amp, 0.5, {
                    value: target
                })
            }

            click() {
                let volTarget = this.soundFlag == true ? 0 : 1
                let target = this.soundFlag == true ? 3 : 30
                if (this.soundFlag == false)
                    soundReactor.audio.currentTime = window.localStorage.getItem('audioTime') || 0

                if (soundReactor.audio.duration > 0 && !soundReactor.audio.paused) {
                    window.localStorage.setItem('audioWasPlaying', false)
                } else {
                    soundReactor.audio.volume = 0
                    soundReactor.audio.play()
                    window.localStorage.setItem('audioWasPlaying', true)
                }

                TweenLite.to(this.amp, 0.5, {
                    value: target
                })
                TweenLite.to(soundReactor.audio, 0.5, {
                    volume: volTarget,
                    onComplete: function () {
                        if (volTarget == 0) {
                            window.localStorage.setItem('audioTime', soundReactor.audio.currentTime)
                            soundReactor.audio.pause()
                        }
                    }
                })
                this.soundFlag = this.soundFlag == true ? false : true;
            }

            onResize() {
                this.sTWidth = this.ctx.canvas.width = this.container.offsetWidth * dpi
                this.sTHeight = this.ctx.canvas.height = this.container.offsetHeight * dpi
                this.ctx.lineWidth = dpi
            }

            bind() {
                this.started = this.started.bind(this)
                this.update = this.update.bind(this)
                this.click = this.click.bind(this)
                this.onResize = this.onResize.bind(this)
                this.mouseMove = this.mouseMove.bind(this)
            }
        }

        class SoundReactor {
            constructor(audioUrl, pageType) {
                this.pageType = pageType
                this.ctx
                this.audio
                this.audioSource
                this.analyser
                this.fdata = []
                this.url = audioUrl

                this.bind()
            }

            init() {
                var AudioContext = window.AudioContext // Default
                    ||
                    window.webkitAudioContext // Safari and old versions of Chrome
                    ||
                    false;
                this.ctx = new AudioContext();
                this.audio = new Audio(this.url);
                this.audio.loop = true;
                if (this.pageType != 'home')
                    return
                this.audioSource = this.ctx.createMediaElementSource(this.audio);
                this.analyser = this.ctx.createAnalyser();
                this.analyser.smoothingTimeConstant = 0.4

                this.audioSource.connect(this.analyser);
                this.audioSource.connect(this.ctx.destination);
                this.fdata = new Uint8Array(this.analyser.frequencyBinCount);

            }

            update() {
                this.analyser.getByteFrequencyData(this.fdata);
            }

            bind() {
                this.update = this.update.bind(this)
                this.init = this.init.bind(this)
            }
        }

        class RAFClass {
            constructor() {
                this.bind()
                this.callbacks = []
                this.dt = 0.15
                this.lastF = Date.now()
                this.render()

            }

            subscribe(name, callback) {
                this.callbacks.push({
                    name: name,
                    callback: callback
                })
            }

            unsubscribe(name) {
                this.callbacks.forEach((item, i) => {
                    if (item.name == name)
                        this.callbacks.splice(i, i + 1)
                });
            }

            render() {
                requestAnimationFrame(this.render)
                this.callbacks.forEach(item => {
                    item.callback()
                });

                this.dt = Date.now() - this.lastF
                this.lastF = Date.now()
            }

            bind() {
                this.subscribe = this.subscribe.bind(this)
                this.unsubscribe = this.unsubscribe.bind(this)
                this.render = this.render.bind(this)

            }
        }

        const RAF = new RAFClass()
    </script>

    <style>
        *,
        *::after,
        *::before {
            box-sizing: border-box;
        }

        .nav {
            z-index: 999;
        }

        .nav-trigger {
            pointer-events: none;
        }

        body {
            -webkit-tap-highlight-color: transparent;
            -webkit-font-smoothing: subpixel-antialiased;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            -webkit-overflow-scrolling: touch;
            -webkit-overflow-scrolling: touch;
            scrollbar-width: none;
            overscroll-behavior: none;
        }

        /*Disable Text Selection */
        @media (min-width: 991px) {
            * {
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }
        }

        input,
        textarea {
            -webkit-user-select: text;
            -khtml-user-select: text;
            -moz-user-select: text;
            -ms-user-select: text;
            user-select: text;
        }

        /*Remove Scrollbar */
        ::-webkit-scrollbar {
            -ms-overflow-style: none;
            display: none;
            overflow: -moz-scrollbars-none;
            scrollbar-width: none;
            -ms-overflow-style: none;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }


        .burgercontainer canvas {
            width: 100%;
            height: 100%;
        }

        .soundtoggler canvas {
            width: 100%;
            height: 100%;
        }

        .menu {
            background-color: transparent;
        }

        .nav-trigger {
            pointer-events: none;
        }

        .nav-trigger.on {
            pointer-events: all;
        }

        .burgerclickablein,
        .burgerclickableout {
            display: none;
        }

        .burgerclickablein.on,
        .burgerclickableout.on {
            display: block;
        }

        .trigger.burgercontainer {
            pointer-events: none;
        }

        .cursorcontainer canvas {
            width: 100%;
            height: 100%;
        }

        .cursorcontainer {
            pointer-events: none;
            display: block !important;

        }
    </style>
    <style>
        @media (min-width: 991px) {
            .a-quote {
                -webkit-text-stroke: 0.8px #0a0a0a;
                -webkit-text-fill-color: transparent;

            }

            .a-span {
                -webkit-text-stroke: transparent;
                -webkit-text-fill-color: #0a0a0a;
            }

            .a-client {
                -webkit-text-stroke: .8px #0D0D0D;
                -webkit-text-fill-color: transparent;
            }

            .a-client:hover {
                -webkit-text-stroke: transparent;
                -webkit-text-fill-color: #0D0D0D;
            }
        }


        @media (max-width: 991px) {
            .a-quote {
                -webkit-text-stroke: 0.4px #0a0a0a;
                -webkit-text-fill-color: transparent;

            }

            .a-span {
                -webkit-text-stroke: transparent;
                -webkit-text-fill-color: #0a0a0a;
            }

            .a-client {
                -webkit-text-stroke: .6px #0D0D0D;
                -webkit-text-fill-color: transparent;
            }

            .a-client.ac {
                -webkit-text-stroke: transparent;
                -webkit-text-fill-color: #0a0a0a;
            }

        }

        @media (max-width: 479px) {
            .a-quote {
                -webkit-text-stroke: 0.6px #0a0a0a;
                -webkit-text-fill-color: transparent;

            }

            .a-span {
                -webkit-text-stroke: transparent;
                -webkit-text-fill-color: #0a0a0a;
            }

            .a-client {
                -webkit-text-stroke: .4px #0D0D0D;
                -webkit-text-fill-color: transparent;
            }

            .a-client.ac {
                -webkit-text-stroke: transparent;
                -webkit-text-fill-color: #0a0a0a;
            }


        }
    </style>
</head>
<body class='body' style="background-color: white">
<div class='trigger burgerclickableout'></div>
<div class='cursor cursorcontainer'></div>
<div class='rotate'>
    <div class='text-block-9'>Rotate your device, please.</div>
</div>
<main data-scroll-container='null' class='app'>
    <div class='page'>
        <div class='about' style="padding: 0">
            <div data-w-id='bbe6450a-41b8-7704-3e3b-779b83b2b441' class='a-slider'>
                <div class='a-marquee'>
                    <div data-scroll='0' data-scroll-direction='horizontal' data-scroll-speed='8' data-w-id='bbe6450a-41b8-7704-3e3b-779b83b2b443'
                         class='marquee'>
                        <h2 class='a-client'>Libera tu espíritu.</h2>
                    </div>
                    <div data-scroll='0' data-scroll-direction='horizontal' data-scroll-speed='8' data-w-id='bbe6450a-41b8-7704-3e3b-779b83b2b443'
                         class='marquee'>
                        <h2 class='a-client ac'>El camino del té.</h2>
                    </div>
                    <div data-scroll='0' data-scroll-direction='horizontal' data-scroll-speed='-7' data-w-id='b70f1eab-a44b-bd23-9e85-8b3fb7e23fe4'
                         class='marquee'>
                        <h2 class='a-client'>es el desarrollo del ser</h2>
                    </div>
                </div>
                <div class='a-client-w'><h2 class='a-client-m'>sky</h2>
                    <h2 class='a-client-m'>Lavazza</h2>
                    <h2 class='a-client-m'>Moët &amp; Chandon</h2>
                    <h2 class='a-client-m'>Diesel</h2>
                    <h2 class='a-client-m'>Swatch</h2>
                    <h2 class='a-client-m'>Alessi</h2>
                    <h2 class='a-client-m'>Martini</h2>
                    <h2 class='a-client-m'>Valentino</h2>
                    <h2 class='a-client-m'>Olivetti</h2>
                    <h2 class='a-client-m'>Costa &amp; More</h2></div>
            </div>
        </div>
    </div>
</main>
<script src='https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5ea69b4027484b2df2b45806' type='text/javascript'
        integrity='sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=' crossorigin='anonymous'></script>
<script src='https://uploads-ssl.webflow.com/5ea69b4027484b2df2b45806/js/webflow.fcbda2e35.js' type='text/javascript'></script>
<!--[if lte IE 9]>
<script src='//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js'></script><![endif]-->
<script>
    let dpi = window.devicePixelRatio || 1;
    window.onload = function () {
        !function () {
            const e = document.querySelectorAll('.pagelink'), t = document.createElement('canvas');
            t.style = "position: fixed;\n              top: 0;\n              left: 0;\n              width: 100vw;\n              height: 100vh;\n              z-index: 2000;\n              pointer-events: none;", document.body.appendChild(t);
            const n = t.getContext('2d');
            let o = n.canvas.width = window.innerWidth, i = n.canvas.height = window.innerHeight, a = '#FFAAFF', l = 1.2, u = !1, c = {value: 1}, d = {value: 1};

            function v(e) {
                e.preventDefault();
                let t = this.getAttribute('href');
                a = '#E5E3DC', c.value = 1, d.value = 1, u = !1, setTimeout(function () {
                    document.location.href = t
                }, 1e3 * l), null == t && (t = 'index.html'), window.isMobile || TweenLite.to(soundReactor.audio, l, {volume: 0}), TweenLite.to(d, l, {
                    value: 0,
                    ease: Power4.easeInOut,
                    onComplete: function () {
                        u = !0
                    }
                }), TweenLite.to(c, l, {
                    value: 0, ease: Power4.easeInOut, delay: l, onComplete: function () {
                        window.localStorage.setItem('audioTime', soundReactor.audio.currentTime)
                    }
                })
            }

            RAF.subscribe('overlayUpdate', function () {
                n.clearRect(0, 0, o, i), n.beginPath(), n.fillStyle = a, n.rect(0, 0, o, i), u && n.fill(), n.closePath(), n.beginPath(), n.moveTo(0, i * c.value), n.quadraticCurveTo(o / 2, i * c.value + 300 * Math.sin(c.value * Math.PI), o, i * c.value), n.lineTo(o, i * d.value), n.quadraticCurveTo(o / 2, i * d.value + -300 * Math.sin(d.value * Math.PI), 0, i * d.value), n.lineTo(0, 0), n.closePath(), n.fillStyle = '#0D0D0D', n.fill()
            }), e.forEach(e => {
                e.addEventListener('click', v)
            })
        }()
    };
</script>

<script>
    if ($(window).width() >= 991) {
        function initSmoothScroll() {
            let o, e = document.querySelectorAll('.parazoom'), t = 0, i = [];
            e.forEach(o => {
                i.push({el: o, width: o.width, height: 0})
            }), setTimeout(function () {
                window.scrollTo(0, 0)
            }, 100), console.log(window.scrollY), window.onbeforeunload = function () {
                window.scrollTo(0, 0)
            }, (o = navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ? new LocomotiveScroll({
                el: document.querySelector('[data-scroll-container]'),
                smooth: !0,
                multiplier: 5
            }) : new LocomotiveScroll({el: document.querySelector('[data-scroll-container]'), smooth: !0})).on('scroll', function (o) {
                t = o.scroll.y, i.forEach(e => {
                    console.log(e.height);
                    let i = e.el.getAttribute('zoomIntensity') || .3;
                    e.el.style.width = e.width + e.width * (t / o.limit) * i + 'px', e.el.style.height = e.height + e.height * (t / o.limit) * i + 'px'
                })
            }), o.on('call', function (o) {
                console.log(o)
            }), window.addEventListener('resize', function () {
                e.forEach(o => {
                    o.el = o, o.width = o.offsetWidth, o.height = o.offsetHeight
                })
            })
        }

        const o = document.querySelector('.app');
        null != o && (o.style.position = 'fixed'), document.addEventListener('DOMContentLoaded', function () {
            setTimeout(function () {
                initSmoothScroll(), console.log('init')
            }, 1e3)
        })
    }
</script>

<script>
    const burgerBP = [{sWidth: 1920, burgerRad: 60 * dpi, burgerMargin: 100 * dpi, burgerBigRad: 90 * dpi, burgerBigMargin: 140 * dpi}, {
        sWidth: 991,
        burgerRad: 48 * dpi,
        burgerMargin: 90 * dpi,
        burgerBigRad: 80 * dpi,
        burgerBigMargin: 120 * dpi
    }, {sWidth: 479, burgerRad: 30 * dpi, burgerMargin: 50 * dpi, burgerBigRad: 60 * dpi, burgerBigMargin: 100 * dpi}, {
        sWidth: 0,
        burgerRad: 30 * dpi,
        burgerMargin: 50 * dpi,
        burgerBigRad: 60 * dpi,
        burgerBigMargin: 100 * dpi
    }];

    function burgerInit() {
        let e = !0;
        const r = new SimplexNoise;
        let i = {
            burgerRad: 40,
            burgerMargin: 60,
            burgerBigRad: 80,
            burgerBigMargin: 120,
            burgerPosition: {x: window.innerWidth * dpi, y: 0},
            circleNumber: 2,
            noiseAmplitude: 5,
            noiseDetail: .5,
            noiseSpeed: .001,
            buttonAlpha: 1
        }, t = !0;
        burgerBP.forEach((e, r) => {
            t && window.innerWidth > e.sWidth && (i.burgerRad = e.burgerRad, i.burgerMargin = e.burgerMargin, i.burgerBigRad = e.burgerBigRad, i.burgerBigMargin = e.burgerBigMargin, t = !1)
        }), i.burgerPosition.x = window.innerWidth * dpi - i.burgerMargin, i.burgerPosition.y = i.burgerMargin;
        let a = JSON.parse(JSON.stringify(i));
        const n = document.querySelectorAll('.menulink');
        let u = '#FFAAFF', o = !1;
        const g = document.querySelector('.burgercontainer'), s = document.querySelector('.burgerclickablein'),
            l = document.querySelector('.burgerclickableout');
        s.classList.add('on');
        const d = document.createElement('canvas');
        g.appendChild(d);
        const b = d.getContext('2d');
        let c = b.canvas.width = g.offsetWidth * dpi, v = b.canvas.height = g.offsetHeight * dpi, w = 20 * dpi, h = 5 * dpi,
            p = [{value: 10 - w / 2}, {value: -h / 2}, {value: w / 2}, {value: -h / 2}], P = [{value: -w / 2}, {value: h / 2}, {value: w / 2}, {value: h / 2}],
            M = JSON.parse(JSON.stringify(p)), R = JSON.parse(JSON.stringify(P));
        const f = 1.2;
        let T = !1, m = {value: 0}, L = {value: 1}, y = {value: 0};

        function I() {
            !T && e ? (s.classList.remove('on'), e = !1, T = !0, i.burgerMargin = i.burgerBigMargin, TweenLite.to(i.burgerPosition, f, {
                x: c - i.burgerMargin,
                y: i.burgerMargin,
                ease: Power4.easeInOut,
                onComplete: function () {
                    setTimeout(function () {
                        l.classList.add('on'), e = !0
                    }, 500)
                }
            }), TweenLite.to(m, f, {value: 255, ease: Power4.easeInOut}), TweenLite.to(y, f, {
                value: 1,
                ease: Power4.easeInOut
            }), TweenLite.to(i, f, {burgerRad: i.burgerBigRad, ease: Power4.easeInOut}), TweenLite.to(p[0], f, {
                value: -w / 2,
                ease: Power4.easeInOut
            }), TweenLite.to(p[1], f, {value: +w / 2, ease: Power4.easeInOut}), TweenLite.to(p[2], f, {
                value: +w / 2,
                ease: Power4.easeInOut
            }), TweenLite.to(p[3], f, {value: -w / 2, ease: Power4.easeInOut}), TweenLite.to(P[0], f, {
                value: -w / 2,
                ease: Power4.easeInOut
            }), TweenLite.to(P[1], f, {value: -w / 2, ease: Power4.easeInOut}), TweenLite.to(P[2], f, {
                value: +w / 2,
                ease: Power4.easeInOut
            }), TweenLite.to(P[3], f, {
                value: +w / 2,
                ease: Power4.easeInOut
            }), document.querySelector('.nav-trigger').classList.add('on')) : e && (l.classList.remove('on'), e = !1, T = !1, setTimeout(function () {
                i.burgerMargin = a.burgerMargin, TweenLite.to(i.burgerPosition, f, {
                    x: c - i.burgerMargin,
                    y: i.burgerMargin,
                    ease: Power4.easeInOut,
                    onComplete: function () {
                        setTimeout(function () {
                            s.classList.add('on'), e = !0
                        }, 500)
                    }
                }), TweenLite.to(m, f, {value: 0, ease: Power4.easeInOut}), TweenLite.to(y, f, {
                    value: 0,
                    ease: Power4.easeInOut
                }), TweenLite.to(i, f, {burgerRad: a.burgerRad, ease: Power4.easeInOut}), p.forEach((e, r) => {
                    TweenLite.to(e, f, {value: M[r].value, ease: Power4.easeInOut})
                }), P.forEach((e, r) => {
                    TweenLite.to(e, f, {value: R[r].value, ease: Power4.easeInOut})
                }), document.querySelector('.nav-trigger').classList.remove('on')
            }, 500))
        }

        let B = 2e3;

        function O(e) {
            e.preventDefault();
            let r = this.getAttribute('href');
            console.log(r), setTimeout(function () {
                document.location.href = r
            }, 1e3 * f), u = this.getAttribute('targetcolor'), o = !0, T = !1, null == r && (r = 'index.html'), TweenLite.to(i, .5, {buttonAlpha: 0}), TweenLite.to(y, f, {
                value: 0,
                ease: Power4.easeInOut,
                onComplete: function () {
                    null != soundReactor.audio && window.localStorage.setItem('audioTime', soundReactor.audio.currentTime)
                }
            }), null != soundReactor.audio && TweenLite.to(soundReactor.audio, f, {volume: 0})
        }

        s.addEventListener('click', I), l.addEventListener('click', I), window.isMobile || window.addEventListener('mousemove', function (e) {
            let r = [e.clientX, e.clientY], t = Math.sqrt(Math.pow(i.burgerPosition.x - r[0], 2) + Math.pow(i.burgerPosition.y - r[1], 2));
            t <= i.burgerRad && B > i.burgerRad && (document.body.style.cursor = 'pointer', clickFlag = !0, d.classList.add('on'), TweenLite.to(L, .2, {value: 0})), t >= i.burgerRad && B < i.burgerRad && (document.body.style.cursor = 'inherit', clickFlag = !1, d.classList.remove('on'), TweenLite.to(L, .2, {value: 1})), B = t
        }), window.addEventListener('resize', function () {
            t = !0, burgerBP.forEach((e, r) => {
                t && window.innerWidth > e.sWidth && (i.burgerRad = e.burgerRad, i.burgerMargin = e.burgerMargin, i.burgerBigRad = e.burgerBigRad, i.burgerBigMargin = e.burgerBigMargin, t = !1)
            }), a.burgerMargin = i.burgerMargin, a.burgerRad = i.burgerRad, T && (i.burgerMargin = i.burgerBigMargin, i.burgerRad = i.burgerBigRad), i.burgerPosition.x = window.innerWidth - i.burgerMargin, i.burgerPosition.y = i.burgerMargin, c = b.canvas.width = g.offsetWidth * dpi, v = b.canvas.height = g.offsetHeight * dpi, i.burgerPosition.x = c - i.burgerMargin
        }), n.forEach(e => {
            e.addEventListener('click', O)
        }), RAF.subscribe('burgerUpdate', function () {
            b.clearRect(0, 0, c, v), b.lineWidth = dpi, b.beginPath(), b.fillStyle = u, b.rect(0, 0, c, v), o && b.fill(), b.closePath(), b.fillStyle = '#0A0A0A', b.beginPath(), b.moveTo(0, v * y.value), T ? b.quadraticCurveTo(c / 2, v * y.value + 300 * Math.sin(y.value * Math.PI), c, v * y.value) : b.quadraticCurveTo(c / 2, v * y.value - 300 * Math.sin(y.value * Math.PI), c, v * y.value), b.lineTo(c, 0), b.lineTo(0, 0), b.closePath(), b.fillStyle = '#0A0A0A', b.fill(), function () {
                for (let t = 0; t < i.circleNumber; t++) {
                    b.beginPath();
                    for (let a = 0; a < 2 * Math.PI; a += 2 * Math.PI / 50) {
                        let n = Math.cos(a), u = Math.sin(a);
                        var e = r.noise3D(n * i.noiseDetail, u * i.noiseDetail, Date.now() * i.noiseSpeed + t);
                        let o = i.burgerRad + e * i.noiseAmplitude * L.value;
                        n = n * o + i.burgerPosition.x, u = u * o + i.burgerPosition.y, 0 == a ? b.moveTo(n, u) : b.lineTo(n, u)
                    }
                    let a = 1;
                    0 == t && (a = .6), b.strokeStyle = `rgba( ${m.value}, ${m.value}, ${m.value}, ${a * i.buttonAlpha})`, b.closePath(), b.stroke()
                }
            }(), b.beginPath(), b.moveTo(p[0].value + i.burgerPosition.x, p[1].value + i.burgerPosition.y), b.lineTo(p[2].value + i.burgerPosition.x, p[3].value + i.burgerPosition.y), b.closePath(), b.stroke(), b.beginPath(), b.moveTo(P[0].value + i.burgerPosition.x, P[1].value + i.burgerPosition.y), b.lineTo(P[2].value + i.burgerPosition.x, P[3].value + i.burgerPosition.y), b.closePath(), b.stroke(), s.style = `\n            width: ${2 * i.burgerRad / dpi}px;\n            height:${2 * i.burgerRad / dpi}px;\n            top: ${i.burgerMargin / (3 * dpi)}px;\n            right: ${i.burgerMargin / (3 * dpi)}px;`, l.style = `\n            width: ${2 * i.burgerRad / dpi}px;\n            height:${2 * i.burgerRad / dpi}px;\n             top: ${i.burgerMargin / (3 * dpi)}px;\n            right: ${i.burgerMargin / (3 * dpi)}px;`
        })
    }

    burgerInit();
</script>
<script>
    let soundToggler = null
    if (!window.isMobile)
        soundToggler = new SoundToggler()
</script>

<script>
    let customCursor
    if (!window.isMobile) {
        customCursor = new CustomCursor()
        customCursor.customCursorInit(document.querySelector('.cursorcontainer'))
        const links = document.querySelectorAll('a')
        links.forEach(a => {
            a.addEventListener('mouseenter', customCursor.aIn)
            a.addEventListener('mouseleave', customCursor.aOut)
        });
    }
    window.onpageshow = function (event) {
        if (event.persisted) {
            window.location.reload();
        }
    };
</script>
<script>
    const soundReactor = new SoundReactor(
        'https://cdn.jsdelivr.net/gh/niccolomiranda/chiara-luzzana/sound/mainSound.mp3')
    if (!window.isMobile) {
        soundReactor.init()
        soundReactor.audio.crossOrigin = 'anonymous';
    }
</script>
</body>
<!-- Mirrored from www.chiaraluzzana.com/about by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Feb 2022 22:02:55 GMT -->
</html>
