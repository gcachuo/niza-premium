<?php
/*
Té Blanco
Té Verde
Té Negro
Té Pu-her
Té Oolong
Té Rooibos
Té Medicinal
Té Artístico
Tisana Herbal
Tisana Frutal
Blue Life
 */
$categories = [
    ['name' => 'Té Blanco', 'img' => '../assets/img/te-blanco.jpg', 'url' => '../camellia-sinensis'],
    ['name' => 'Té Verde', 'img' => '../assets/img/te-verde.jpg', 'url' => '../camellia-sinensis'],
    ['name' => 'Té Negro', 'img' => '../assets/img/te-negro.jpeg', 'url' => '../camellia-sinensis'],
    ['name' => 'Té Pu-her', 'img' => '../assets/img/te-puher.jpg', 'url' => '../camellia-sinensis'],
    ['name' => 'Té Oolong', 'img' => '../assets/img/te-oolong.jpg', 'url' => '../camellia-sinensis'],
    ['name' => 'Té Medicinal', 'img' => '../assets/img/te-medicinal.jpg', 'url' => '../te-herbal-y-frutal'],
    ['name' => 'Tisana Rooibos', 'img' => '../assets/img/tisana-rooibos.jpg', 'url' => '../te-herbal-y-frutal'],
    ['name' => 'Tisana Herbal', 'img' => '../assets/img/tisana-herbal.jpg', 'url' => '../te-herbal-y-frutal'],
    ['name' => 'Tisana Frutal', 'img' => '../assets/img/tisana-frutal.jpg', 'url' => '../te-herbal-y-frutal'],
    ['name' => 'Infusión Azul', 'img' => '../assets/img/infusion-azul.jpg', 'url' => '../te-herbal-y-frutal']
];
?>

<!-- Mirrored from themarcus.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Feb 2022 04:56:18 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <script type="text/javascript" crossorigin="anonymous" defer="defer" nomodule="nomodule"
            src="http://assets.squarespace.com/@sqs/polyfiller/1.2.2/legacy.js"></script>
    <script type="text/javascript" crossorigin="anonymous" defer="defer"
            src="http://assets.squarespace.com/@sqs/polyfiller/1.2.2/modern.js"></script>
    <script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
    <script>(function (rollups, name) {
            if (!rollups[name]) {
                rollups[name] = {};
            }
            rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/style-loader-runtime-c9f4db644d8097f2ec4cb-min.en-US.js"];
        })(SQUARESPACE_ROLLUPS, 'squarespace-style_loader_runtime');</script>
    <script crossorigin="anonymous"
            src="http://assets.squarespace.com/universal/scripts-compressed/style-loader-runtime-c9f4db644d8097f2ec4cb-min.en-US.js"
            defer></script>
    <script>(function (rollups, name) {
            if (!rollups[name]) {
                rollups[name] = {};
            }
            rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/extract-css-runtime-090db81fcd92b5d966055-min.en-US.js"];
        })(SQUARESPACE_ROLLUPS, 'squarespace-extract_css_runtime');</script>
    <script crossorigin="anonymous"
            src="http://assets.squarespace.com/universal/scripts-compressed/extract-css-runtime-090db81fcd92b5d966055-min.en-US.js"
            defer></script>
    <script>(function (rollups, name) {
            if (!rollups[name]) {
                rollups[name] = {};
            }
            rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/moment-js-vendor-a0e053b59aa8d604fd366-min.en-US.js"];
        })(SQUARESPACE_ROLLUPS, 'squarespace-moment_js_vendor');</script>
    <script crossorigin="anonymous"
            src="http://assets.squarespace.com/universal/scripts-compressed/moment-js-vendor-a0e053b59aa8d604fd366-min.en-US.js"
            defer></script>
    <script>(function (rollups, name) {
            if (!rollups[name]) {
                rollups[name] = {};
            }
            rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d203c83bbf2fcd50ff9cb-min.en-US.js"];
        })(SQUARESPACE_ROLLUPS, 'squarespace-cldr_resource_pack');</script>
    <script crossorigin="anonymous"
            src="http://assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d203c83bbf2fcd50ff9cb-min.en-US.js"
            defer></script>
    <script>(function (rollups, name) {
            if (!rollups[name]) {
                rollups[name] = {};
            }
            rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-stable-28fe0d11d2c4af18f3a5e-min.en-US.js"];
        })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors_stable');</script>
    <script crossorigin="anonymous"
            src="http://assets.squarespace.com/universal/scripts-compressed/common-vendors-stable-28fe0d11d2c4af18f3a5e-min.en-US.js"
            defer></script>
    <script>(function (rollups, name) {
            if (!rollups[name]) {
                rollups[name] = {};
            }
            rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-cd127d6f535d331194715-min.en-US.js"];
        })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors');</script>
    <script crossorigin="anonymous"
            src="http://assets.squarespace.com/universal/scripts-compressed/common-vendors-cd127d6f535d331194715-min.en-US.js"
            defer></script>
    <script>(function (rollups, name) {
            if (!rollups[name]) {
                rollups[name] = {};
            }
            rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-4edf38288aea05e9449c9-min.en-US.js"];
        })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
    <script crossorigin="anonymous"
            src="http://assets.squarespace.com/universal/scripts-compressed/common-4edf38288aea05e9449c9-min.en-US.js"
            defer></script>
    <script>(function (rollups, name) {
            if (!rollups[name]) {
                rollups[name] = {};
            }
            rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/performance-d706abd15cdfedc1aff7c-min.en-US.js"];
        })(SQUARESPACE_ROLLUPS, 'squarespace-performance');</script>
    <script crossorigin="anonymous"
            src="http://assets.squarespace.com/universal/scripts-compressed/performance-d706abd15cdfedc1aff7c-min.en-US.js"
            defer></script>
    <script data-name="static-context">Static = window.Static || {};
        Static.SQUARESPACE_CONTEXT = {
            "facebookAppId": "314192535267336",
            "facebookApiVersion": "v6.0",
            "rollups": {
                "squarespace-announcement-bar": {"js": "//assets.squarespace.com/universal/scripts-compressed/announcement-bar-85a8b96795915b9e7f84d-min.en-US.js"},
                "squarespace-audio-player": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/audio-player-7273d8fcec67906942b35-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/audio-player-c393c9cff0f987d4dfce9-min.en-US.js"
                },
                "squarespace-blog-collection-list": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/blog-collection-list-3d55c64c25996c7633fc2-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-d777b4d4a1e5a0b52e24c-min.en-US.js"
                },
                "squarespace-calendar-block-renderer": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-5668de53c0ce16e20cc01-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-23dcad7ec2ed717f6fc57-min.en-US.js"
                },
                "squarespace-chartjs-helpers": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-58ae73137091cd0a61360-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-599541a905a8f30f52f33-min.en-US.js"
                },
                "squarespace-comments": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/comments-eeb99f32a31032af774cb-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/comments-4978efae8bb8c7e08c0b3-min.en-US.js"
                },
                "squarespace-dialog": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/dialog-7b3fdec47b80fd63e5e6f-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/dialog-7e20c9a3c0f9b315b9d44-min.en-US.js"
                },
                "squarespace-events-collection": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/events-collection-5668de53c0ce16e20cc01-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/events-collection-5b81f90d14be874ee0ec4-min.en-US.js"
                },
                "squarespace-form-rendering-utils": {"js": "//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-51880a894886ff422f192-min.en-US.js"},
                "squarespace-forms": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/forms-1cc007b21ede0b73086c9-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/forms-39073aad6b1ab489599f9-min.en-US.js"
                },
                "squarespace-gallery-collection-list": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-3d55c64c25996c7633fc2-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-48c9d1006aec2b6ef93c4-min.en-US.js"
                },
                "squarespace-image-zoom": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/image-zoom-60e14b9bac69739c96fa7-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/image-zoom-1aef7d01397062b0578b0-min.en-US.js"
                },
                "squarespace-pinterest": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/pinterest-3d55c64c25996c7633fc2-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/pinterest-fa27ecce216e47c4249c2-min.en-US.js"
                },
                "squarespace-popup-overlay": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/popup-overlay-e4ea05bd2ae9c1568e432-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/popup-overlay-ccca5a3e3a5dbf3762834-min.en-US.js"
                },
                "squarespace-product-quick-view": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/product-quick-view-663fb8b8c08febe7303f1-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/product-quick-view-e60b5e402a63ca2f949cb-min.en-US.js"
                },
                "squarespace-products-collection-item-v2": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-60e14b9bac69739c96fa7-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-c3055d65e385702cdc61c-min.en-US.js"
                },
                "squarespace-products-collection-list-v2": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-60e14b9bac69739c96fa7-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-e42dcafd488a1e0b855e9-min.en-US.js"
                },
                "squarespace-search-page": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/search-page-568ad8f2a40e76c0175c8-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/search-page-3db43a0723a12ec44227a-min.en-US.js"
                },
                "squarespace-search-preview": {"js": "//assets.squarespace.com/universal/scripts-compressed/search-preview-b94131d25eddc639a52e5-min.en-US.js"},
                "squarespace-simple-liking": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/simple-liking-47606e375db2b296c3464-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/simple-liking-1159db08b6e09ca663491-min.en-US.js"
                },
                "squarespace-social-buttons": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/social-buttons-b186d09e02921fd7f8e00-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/social-buttons-763529a4a7472185e446d-min.en-US.js"
                },
                "squarespace-tourdates": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/tourdates-3d55c64c25996c7633fc2-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/tourdates-7af6e456d899245a0204d-min.en-US.js"
                },
                "squarespace-website-overlays-manager": {
                    "css": "//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-5c2f030f6ee94f066dc3d-min.en-US.css",
                    "js": "//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-f3b999a82ae9297f7cd83-min.en-US.js"
                }
            },
            "pageType": 2,
            "website": {
                "id": "5e8f5d9bae8f137fbc83492a",
                "identifier": "porpoise-hexahedron-lys7",
                "websiteType": 1,
                "contentModifiedOn": 1643836851566,
                "cloneable": false,
                "hasBeenCloneable": false,
                "developerMode": true,
                "siteStatus": {},
                "language": "en-US",
                "timeZone": "America/New_York",
                "machineTimeZoneOffset": -18000000,
                "timeZoneOffset": -18000000,
                "timeZoneAbbr": "EST",
                "siteTitle": "Marcus Eriksson",
                "fullSiteTitle": "Marcus Eriksson",
                "siteDescription": "",
                "socialLogoImageId": "60414eb4c13a867fd2577d8e",
                "shareButtonOptions": {"3": true, "1": true, "2": true, "4": true, "8": true, "6": true, "7": true},
                "socialLogoImageUrl": "//images.squarespace-cdn.com/content/v1/5e8f5d9bae8f137fbc83492a/1614892725051-BK1H4MXDNUJ306VVIET1/share.png",
                "authenticUrl": "https://themarcus.com",
                "internalUrl": "https://porpoise-hexahedron-lys7.squarespace.com",
                "baseUrl": "https://themarcus.com",
                "primaryDomain": "themarcus.com",
                "sslSetting": 3,
                "isHstsEnabled": false,
                "typekitId": "",
                "statsMigrated": false,
                "imageMetadataProcessingEnabled": false,
                "screenshotId": "d930f607d83f33cdba330f53d083beb03de724f0af40c6128056426c5c81a10d",
                "captchaSettings": {"enabledForDonations": false},
                "showOwnerLogin": false
            },
            "websiteSettings": {
                "id": "5e8f5d9bae8f137fbc83492d",
                "websiteId": "5e8f5d9bae8f137fbc83492a",
                "subjects": [],
                "country": "US",
                "state": "NJ",
                "simpleLikingEnabled": true,
                "mobileInfoBarSettings": {
                    "isContactEmailEnabled": false,
                    "isContactPhoneNumberEnabled": false,
                    "isLocationEnabled": false,
                    "isBusinessHoursEnabled": false
                },
                "commentLikesAllowed": true,
                "commentAnonAllowed": true,
                "commentThreaded": true,
                "commentApprovalRequired": false,
                "commentAvatarsOn": true,
                "commentSortType": 2,
                "commentFlagThreshold": 0,
                "commentFlagsAllowed": true,
                "commentEnableByDefault": true,
                "commentDisableAfterDaysDefault": 0,
                "disqusShortname": "",
                "commentsEnabled": false,
                "storeSettings": {
                    "returnPolicy": null,
                    "termsOfService": null,
                    "privacyPolicy": null,
                    "expressCheckout": false,
                    "continueShoppingLinkUrl": "/",
                    "useLightCart": false,
                    "showNoteField": false,
                    "shippingCountryDefaultValue": "US",
                    "billToShippingDefaultValue": false,
                    "showShippingPhoneNumber": true,
                    "isShippingPhoneRequired": false,
                    "showBillingPhoneNumber": true,
                    "isBillingPhoneRequired": false,
                    "currenciesSupported": ["USD", "CAD", "GBP", "AUD", "EUR", "CHF", "NOK", "SEK", "DKK", "NZD", "SGD", "MXN", "HKD", "CZK", "ILS", "MYR", "RUB", "PHP", "PLN", "THB", "BRL", "ARS", "COP", "IDR", "INR", "JPY", "ZAR"],
                    "defaultCurrency": "USD",
                    "selectedCurrency": "USD",
                    "measurementStandard": 1,
                    "showCustomCheckoutForm": false,
                    "checkoutPageMarketingOptInEnabled": false,
                    "enableMailingListOptInByDefault": false,
                    "sameAsRetailLocation": false,
                    "merchandisingSettings": {
                        "scarcityEnabledOnProductItems": false,
                        "scarcityEnabledOnProductBlocks": false,
                        "scarcityMessageType": "DEFAULT_SCARCITY_MESSAGE",
                        "scarcityThreshold": 10,
                        "multipleQuantityAllowedForServices": true,
                        "restockNotificationsEnabled": false,
                        "restockNotificationsMailingListSignUpEnabled": false,
                        "relatedProductsEnabled": false,
                        "relatedProductsOrdering": "random",
                        "soldOutVariantsDropdownDisabled": false,
                        "productComposerOptedIn": false,
                        "productComposerABTestOptedOut": false,
                        "productReviewsEnabled": false
                    },
                    "isLive": false,
                    "multipleQuantityAllowedForServices": true
                },
                "useEscapeKeyToLogin": true,
                "ssBadgeType": 1,
                "ssBadgePosition": 4,
                "ssBadgeVisibility": 1,
                "ssBadgeDevices": 1,
                "pinterestOverlayOptions": {"mode": "disabled"},
                "ampEnabled": false
            },
            "cookieSettings": {
                "isCookieBannerEnabled": false,
                "isRestrictiveCookiePolicyEnabled": false,
                "isRestrictiveCookiePolicyAbsolute": false,
                "cookieBannerText": "",
                "cookieBannerTheme": "",
                "cookieBannerVariant": "",
                "cookieBannerPosition": "",
                "cookieBannerCtaVariant": "",
                "cookieBannerCtaText": "",
                "cookieBannerAcceptType": "OPT_IN",
                "cookieBannerOptOutCtaText": ""
            },
            "websiteCloneable": false,
            "collection": {
                "title": "Home",
                "id": "5e8f5ecd4882d92a9da72f97",
                "fullUrl": "/",
                "type": 10,
                "permissionType": 1
            },
            "subscribed": false,
            "appDomain": "squarespace.com",
            "templateTweakable": true,
            "tweakJSON": {
                "maxPageWidth": "1200px",
                "pagePadding": "3vw",
                "product-gallery-auto-crop": "false",
                "product-image-auto-crop": "true",
                "tweak-portfolio-slides-cover-overlay-color": "#38383B",
                "tweak-portfolio-slides-inset-overlay-color": "#38383B",
                "tweak-portfolio-slides-split-overlay-color": "#38383B",
                "tweak-v1-related-products-title-spacing": "50px"
            },
            "templateId": "5e8f5dc001e1e4727920d9c5",
            "templateVersion": "7.1",
            "pageFeatures": [1, 2, 4],
            "gmRenderKey": "QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4",
            "templateScriptsRootUrl": "https://static1.squarespace.com/static/ta/5e8f5d9bae8f137fbc83492a/2012/scripts/",
            "betaFeatureFlags": ["commerce_clearpay", "commerce_onboarding_tools_screen_test", "commerce_activation_experiment_add_payment_processor_card", "member_areas_spanish_interviews", "commerce_site_visitor_metrics", "campaigns_blog_product_image_editor", "google_analytics_4_gtag_js", "commerce_product_composer_ab_test_all_users", "commerce_category_id_discounts_enabled", "category-delete-product-service-enabled", "themes", "campaigns_audience_card", "commerce_etsy_shipping_import", "campaigns_hide_deleted_automations_panel", "reduce_general_search_api_traffic", "background_art_onboarding", "nested_categories", "COMMERCE_PRODUCT_REVIEWS_MERCHANT_EMAIL", "commerce_restock_notifications", "campaigns_thumbnail_layout", "commerce_afterpay_pdp", "customer_account_creation_recaptcha", "site_header_footer", "section_admin", "campaigns_banner_reduce_size", "commerce_fts_review_survey", "campaigns_new_subscriber_search", "override_block_styles", "campaigns_alt_text", "collection_typename_switching", "startup_checklist", "campaigns_content_editing_survey", "viewer-role-contributor-invites", "campaigns_show_apply_website_styles_button", "commerce_etsy_product_import", "new_stacked_index", "commerce_pdp_layouts_ga", "campaigns_global_uc_ab", "product-retriever-v2-enabled", "member_areas_schedule_interview", "campaigns_section_reorder_arrows", "scripts_defer", "commerce_product_branching", "nested_categories_migration_enabled"],
            "impersonatedSession": false,
            "tzData": {
                "zones": [[-300, "US", "E%sT", null]],
                "rules": {"US": [[1967, 2006, null, "Oct", "lastSun", "2:00", "0", "S"], [1987, 2006, null, "Apr", "Sun>=1", "2:00", "1:00", "D"], [2007, "max", null, "Mar", "Sun>=8", "2:00", "1:00", "D"], [2007, "max", null, "Nov", "Sun>=1", "2:00", "0", "S"]]}
            },
            "showAnnouncementBar": false
        };</script>
    <script type="application/ld+json">{
            "url": "https://themarcus.com",
            "name": "Marcus Eriksson",
            "description": "",
            "@context": "http://schema.org",
            "@type": "WebSite"
        }</script>
    <link rel="stylesheet" type="text/css"
          href="../assets/css/themarcus.css"/>
    <script>
        // Must match slugs (url's) exactly
        var PORTFOLIO_CONFIG = [
            {slug: '/portfolio/aaron-judge', color: '#e8ebf2', isLightColor: true, layout: 9, isFeatured: false},
            {slug: '/portfolio/amoeba', color: '#88a9bd', isLightColor: true, layout: 10, isFeatured: true},
            {slug: '/portfolio/catrina-simone', color: '#f8cf14', isLightColor: true, layout: 5, isFeatured: true},
            {slug: '/portfolio/arch-motorcycle', color: '#545454', isLightColor: false, layout: 2, isFeatured: false},
            {slug: '/portfolio/kimberly-dioszeghy', color: '#062fc2', isLightColor: false, layout: 2, isFeatured: true},
            {slug: '/portfolio/oculus-quest', color: '#123690', isLightColor: false, layout: 2, isFeatured: false},
            {slug: '/portfolio/baby-yors', color: '#e6cbf2', isLightColor: true, layout: 9, isFeatured: true},
            {slug: '/portfolio/beats-by-dr-dre', color: '#6b6b6b', isLightColor: false, layout: 6, isFeatured: true},
            {slug: '/portfolio/breanna-stewart', color: '#ebc12a', isLightColor: true, layout: 12, isFeatured: true},
            {slug: '/portfolio/clermont-twins', color: '#ABB4B8', isLightColor: true, layout: 7, isFeatured: true},
            {slug: '/portfolio/corey-seager', color: '#CEBFB0', isLightColor: true, layout: 8, isFeatured: false},
            {slug: '/portfolio/damian-lillard', color: '#faeb8c', isLightColor: true, layout: 7, isFeatured: true},
            {slug: '/portfolio/demar-derozan', color: '#eeeeee', layout: 5, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/emma-coburn', color: '#cfcfcf', layout: 4, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/evander-kane', color: '#96c0a7', isLightColor: true, layout: 8, isFeatured: true},
            {slug: '/portfolio/gilbert-and-claire', color: '#bababa', layout: 5, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/jake-arrieta', color: '#c1e3e2', layout: 2, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/kayla-kyle-moffat', color: '#bd685d', layout: 9, isFeatured: false},
            {slug: '/portfolio/keanu-reeves', color: '#95aba7', layout: 4, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/kenya-and-carmel', color: '#d3ded5', layout: 5, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/kyle-and-demar', color: '#d0d8d1', layout: 6, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/leisure-center', color: '#73bcc5', isLightColor: true, layout: 12, isFeatured: true},
            {slug: '/portfolio/mens-health', color: '#86c5cf', layout: 4, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/nike-fiba', color: '#dfcfcd', isLightColor: true, layout: 11, isFeatured: false},
            {slug: '/portfolio/nike-training-kit', color: '#0036ff', isLightColor: true, layout: 4, isFeatured: true},
            {slug: '/portfolio/nike-training', color: '#596a73', isLightColor: false, layout: 10, isFeatured: false},
            {slug: '/portfolio/sang-jeong-lee', color: '#77c7c4', isLightColor: true, layout: 3, isFeatured: true},
            {slug: '/portfolio/michael-jordan', color: '#4B9CD3', layout: 11, isFeatured: true, isLightColor: true},
            {slug: '/portfolio/dwyane-wade', color: '#828282', layout: 7, isFeatured: true},
            {slug: '/portfolio/carmelo-anthony', color: '#e2e2e2', layout: 7, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/colin-kaepernick', color: '#ede2ca', layout: 7, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/serena-williams', color: '#e3e3e3', layout: 9, isLightColor: true, isFeatured: true},
            {slug: '/portfolio/mike-trout', color: '#7b877e', layout: 9, isLightColor: false, isFeatured: true},
            {slug: '/portfolio/lebron-james', color: '#606060', layout: 9, isLightColor: false, isFeatured: true},
            {slug: '/portfolio/bet-mgm', color: '#0080c9', layout: 4, isLightColor: false, isFeatured: true},
            {slug: '/portfolio/team-rize', color: '#8ae3c8', layout: 2, isLightColor: true, isFeatured: false},
        ]


    </script>
    <script>Static.COOKIE_BANNER_CAPABLE = true;</script>
    <!-- End of Squarespace Headers -->
    <script>
        var ROUTES = [{slug: '/portfolio/amoeba', title: 'AMŒBA'}, {
            slug: '/portfolio/damian-lillard',
            title: 'DAMIAN LILLARD'
        }, {slug: '/portfolio/keanu-reeves', title: 'KEANU REEVES'}, {
            slug: '/portfolio/leisure-center',
            title: 'ALAKIIR DENG'
        }, {slug: '/portfolio/beats-by-dr-dre', title: 'BEATS BY DRE'}, {
            slug: '/portfolio/breanna-stewart',
            title: 'BREANNA STEWART'
        }, {slug: '/portfolio/colin-kaepernick', title: 'Colin Kaepernick'}, {
            slug: '/portfolio/gilbert-and-claire',
            title: 'GILBERT & CLAIRE'
        }, {slug: '/portfolio/bet-mgm', title: 'Bet MGM'}, {slug: '/portfolio/baby-yors', title: 'BABY YORS'}, {
            slug: '/portfolio/kenya-and-carmel',
            title: 'KENYA & CARMEL'
        }, {slug: '/portfolio/evander-kane', title: 'EVANDER KANE'}, {
            slug: '/portfolio/catrina-simone',
            title: 'CATRINA & SIMONE'
        }, {slug: '/portfolio/michael-jordan', title: 'Michael Jordan'}, {
            slug: '/portfolio/nike-training-kit',
            title: 'NIKE TRAINING KIT'
        }, {slug: '/portfolio/kyle-and-demar', title: 'KYLE & DEMAR'}, {
            slug: '/portfolio/clermont-twins',
            title: 'CLERMONT TWINS'
        }, {slug: '/portfolio/mens-health', title: 'Mens Health Magazine'}, {
            slug: '/portfolio/nike-fiba',
            title: 'NIKE FIBA'
        }, {
            slug: '/portfolio/team-rize',
            title: 'RIZE FITNESS'
        }, {slug: '/portfolio/jake-arrieta', title: 'JAKE ARRIETA'}, {
            slug: '/portfolio/corey-seager',
            title: 'COREY SEAGER'
        }, {
            slug: '/portfolio/demar-derozan',
            title: 'DEMAR DEROZAN'
        }, {slug: '/portfolio/kayla-kyle-moffat', title: 'KAYLA KYLE- MOFFAT'}, {
            slug: '/portfolio/sang-jeong-lee',
            title: 'SANG JEONG LEE'
        }, {slug: '/portfolio/emma-coburn', title: 'EMMA COBURN'}, {
            slug: '/portfolio/serena-williams',
            title: 'Serena Williams'
        }, {slug: '/portfolio/kimberly-dioszeghy', title: 'Kimberly Dioszeghy'}, {
            slug: '/portfolio/aaron-judge',
            title: 'AARON JUDGE'
        }, {slug: '/portfolio/oculus-quest', title: 'Oculus Quest'}, {
            slug: '/portfolio/arch-motorcycle',
            title: 'Arch Motorcycle'
        }, {slug: '/portfolio/dwyane-wade', title: 'Dwyane Wade'}, {
            slug: '/portfolio/nike-training',
            title: 'Nike Training'
        }, {slug: '/portfolio/carmelo-anthony', title: 'Carmelo Anthony'}, {
            slug: '/portfolio/mike-trout',
            title: 'Mike Trout'
        }, {slug: '/portfolio/lebron-james', title: 'Lebron James4'},].reduce(function (res, data, i) {
            var color = '#1f1f1f';
            var isLightColor = false;
            var isFeatured = false;
            var layout = 1;
            var match = PORTFOLIO_CONFIG.filter(function (item) {
                return data.slug.indexOf(item.slug) > -1;
            });

            if (match && match[0]) {
                color = match[0].color;
                isLightColor = match[0].isLightColor || false;
                layout = match[0].layout || 1;
                isFeatured = match[0].isFeatured || false;
            }

            res[data.slug] = {
                pageTitle: data.title + ' \u2014 ' + 'Marcus Eriksson',
                title: data.title,
                color: color,
                index: i + 1,
                isLightColor: isLightColor,
                isFeatured: isFeatured,
                layout: layout
            };

            return res;
        }, {
            '/': {
                pageTitle: 'Marcus Eriksson',
                title: 'Marcus Eriksson',
                color: '#ffffff',
                isLightColor: true,
                index: 0
            },
            '/about': {
                pageTitle: 'Marcus Eriksson',
                title: 'Marcus Eriksson',
                color: '#f1f1f1',
                isLightColor: false,
                index: -1
            },
            '/portfolio': {
                pageTitle: 'Marcus Eriksson',
                title: 'Marcus Eriksson',
                color: '#f1f1f1',
                index: -2
            },
            '/#menu': {
                pageTitle: '',
                title: '',
                color: '#ffffff',
                isLightColor: true,
                index: -3
            }
        });
    </script>
</head>

<body id='collection-5e8f5ecd4882d92a9da72f97' data-controller='SiteLoader, Flags'
      data-dynamic-font-sizing-enabled='false'
      class='product-list-titles-under product-list-alignment-center product-item-size-11-square product-image-auto-crop product-gallery-size-11-square  show-product-price show-product-item-nav product-social-sharing tweak-v1-related-products-image-aspect-ratio-11-square tweak-v1-related-products-details-alignment-center newsletter-style-dark hide-opentable-icons opentable-style-dark small-button-style-solid small-button-shape-square medium-button-style-solid medium-button-shape-square large-button-style-solid large-button-shape-square image-block-poster-text-alignment-center image-block-card-dynamic-font-sizing image-block-card-content-position-center image-block-card-text-alignment-center image-block-overlap-dynamic-font-sizing image-block-overlap-content-position-center image-block-overlap-text-alignment-opposite image-block-collage-dynamic-font-sizing image-block-collage-content-position-top image-block-collage-text-alignment-left image-block-stack-dynamic-font-sizing image-block-stack-text-alignment-left button-style-default button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light product-list-titles-under product-list-alignment-center product-item-size-11-square product-image-auto-crop product-gallery-size-11-square  show-product-price show-product-item-nav product-social-sharing tweak-v1-related-products-details-alignment-center native-currency-code-usd collection-type-page collection-layout-home collection-5e8f5ecd4882d92a9da72f97 homepage mobile-style-available sqs-seven-one'>
<div id='siteWrapper' class='clearfix site-wrapper'>

    <div class='floating-cart hidden' data-controller='FloatingCart'>
        <a href='cart.html' class='icon icon--stroke icon--cart sqs-custom-cart'>
            <span class='Cart-inner'>
              <svg class='Icon Icon--cart' viewBox='0 0 31 26'>
                <g class='svg-icon cart-icon--even'>
                  <circle stroke-miterlimit='10' cx='23' cy='23' r='1'/>
                  <circle stroke-miterlimit='10' cx='9' cy='23' r='1'/>
                  <path fill='none' stroke-linejoin='round' stroke-miterlimit='10' d='M0,2h5
                      c0.6,0,1.1,0.4,1.1,1l1.7,13c0.1,0.5,0.6,1,1.1,1h15c0.5,0,1.2-0.4,1.4-0.9l3.3-8.1C28.8,7.4,28.5,7,28,7H12'/>
                </g>

                <g class='svg-icon cart-icon--odd'>
                  <circle stroke-miterlimit='10' cx='22.5' cy='21.5' r='1'/>
                  <circle stroke-miterlimit='10' cx='9.5' cy='21.5' r='1'/>
                  <path fill='none' stroke-miterlimit='10' d='M0,1.5h5c0.6,0,1.1,0.4,1.1,1l1.7,13
                    c0.1,0.5,0.6,1,1.1,1h15c0.5,0,1.2-0.4,1.4-0.9l3.3-8.1c0.2-0.5-0.1-0.9-0.6-0.9H12'/>
                </g>
              </svg>
              <div class='icon-cart-quantity'>
                <span class='sqs-cart-quantity'>0</span>
              </div>
            </span>
        </a>
    </div>


    <main id='page' class='container' role='main' data-controller='MarcusEriksson'>
        <div id='page-content'>
            <div class='me-home' data-controller='Home'>
                <div class='all-projects'>
                    <div class='all-projects-viewport' data-scroll-route='/#menu'></div>
                    <div class='all-projects-wrapper'></div>
                </div>
            </div>
        </div>
    </main>
    <div id='page-interface' class='page-interface'>
        <div class='dynamic-background'></div>

        <div class='parallax-title parallax-title--back parallax-title--menu' data-title='Mas Vendidos'
             data-route='/#menu' arial-hidden='true'
             data-disabled='true'>
            Mas Vendidos
        </div>

        <div class='parallax-title parallax-title--front parallax-title--menu'
             data-title='presiona para conocer nuestros productos' data-route='/#menu'
             data-menu='true' role='button'>
            Mas Vendidos
        </div>

        <div class='page-interface-load'></div>
    </div>

    <div class='menu-index'>
        <div class='menu-index-background'></div>
        <div class='menu-index-data' aria-hidden='true'>

            <?php foreach ($categories as $item): ?>
                <div class='menu-index-data-item' data-route='/portfolio/oculus-quest'
                     data-title='<?= $item['name'] ?>'>
                    <div class='menu-index-image'>
                        <img data-src='<?= $item['img'] ?>'
                             data-image='<?= $item['img'] ?>'
                             data-image-dimensions='2500x2162' data-image-focal-point='0.5,0.5'
                             alt='<?= $item['name'] ?>' data-load='false'/>
                    </div>
                    <a target='_parent' class='menu-index-link' href='<?= $item['url'] ?>' role='button'>
                        <div class='menu-index-link-content'>
                            <span class='menu-index-link-title'><?= $item['name'] ?></span>
                            <span class='menu-index-link-index'>00</span>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>

        </div>
        <div class='menu-index-wrapper'>
            <div class='menu-index-images'>

            </div>
            <div class='menu-index-links'>

            </div>
        </div>

        <div class='menu-index-nav' style="display: none">
            <a class='menu-index-close menu-index-close--back switch-button' arial-hidden='true'>
                <div class='switch-button-inner'>
                    <div class='switch-button-hover'>
                        <div class='switch-button-link' data-title='Close'>Close</div>
                    </div>
                </div>
            </a>

            <a class='menu-index-close menu-index-close--front switch-button' role='button'>
                <div class='switch-button-inner'>
                    <div class='switch-button-hover'>
                        <div class='switch-button-link' data-title='Close'>Close</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class='about' data-site-about>
        <div class='about-background'></div>

        <div class='about-content-wrapper'>
            <div class='about-content'>

            </div>

            <div class='about-nav'>
                <a class='about-close about-close--back switch-button' arial-hidden='true'>
                    <div class='switch-button-inner'>
                        <div class='switch-button-hover'>
                            <div class='switch-button-link' data-title='Close'>Close</div>
                        </div>
                    </div>
                </a>

                <a class='about-close about-close--front switch-button' role='button'>
                    <div class='switch-button-inner'>
                        <div class='switch-button-hover'>
                            <div class='switch-button-link' data-title='Close'>Close</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class='about-background'>
            <video autoplay loop muted playsinline>
                <source data-src='https://static1.squarespace.com/static/ta/5e8f5d9bae8f137fbc83492a/2011/assets/aboutbg.mp4'>
            </video>
        </div>
    </div>

    <div class='lightbox' data-site-lightbox>
        <div class='lightbox-background'></div>
        <div class='lightbox-display'></div>
        <div class='lightbox-cursor lightbox-cursor--back'>
            <div class='lightbox-cursor-chevron'></div>
        </div>
        <div class='lightbox-cursor lightbox-cursor--front'>
            <div class='lightbox-cursor-chevron'></div>
        </div>
    </div>
    <script type='text/javascript'>
        const firstSection = document.querySelector('.page-section');
        const header = document.querySelector('.header');
        const mobileOverlayNav = document.querySelector('.header-menu');
        const sectionBackground = firstSection ? firstSection.querySelector('.section-background') : null;
        const headerHeight = header ? header.getBoundingClientRect().height : 0;
        const firstSectionHasBackground = firstSection ? firstSection.className.indexOf('has-background') >= 0 : false;
        const isFirstSectionInset = firstSection ? firstSection.className.indexOf('background-width--inset') >= 0 : false;
        const isLayoutEngineSection = firstSection ? firstSection.className.indexOf('layout-engine-section') >= 0 : false;

        if (firstSection) {
            firstSection.style.paddingTop = headerHeight + 'px';
        }
        if (sectionBackground && isLayoutEngineSection) {
            if (isFirstSectionInset) {
                sectionBackground.style.top = headerHeight + 'px';
            } else {
                sectionBackground.style.top = '';
            }
        }
        //# sourceURL=headerPositioning.js
    </script>


    <footer class='sections' id='footer-sections' data-footer-sections>

    </footer>

</div>

<script defer='defer' src='../assets/js/themarcus.js' type='text/javascript'></script>
<script type='text/javascript' data-sqs-type='imageloader-bootstrapper'>
    (function () {
        if (window.ImageLoader) {
            window.ImageLoader.bootstrap({}, document);
        }
    })();
    window.addEventListener('load', function () {
        document.getElementsByClassName('parallax-title parallax-title--front parallax-title--menu')[0].click();
    })
</script>
</body>
