<section data-scroll-container>
    <section class="blockDorado3 blockPosition dor-bg-white" data-scroll-section>
        <div class='container'>
            <div class='row'>
                <div style="">
                    <div class='tiny-slider'>
                        <video loop autoplay muted>
                            <source src='assets/video/STORIE%20CARAJILLO%20NIZA.mp4' type='video/mp4'>
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class='dorBizproduct blockPosition dor-bg-gray' data-scroll-section>
        <div class='container'>
            <div class="row">
                <img src='assets/img/home/hojadeoro.jpg' alt=''>
            </div>
            <div class='row' style="margin: 50px 0 50px 0">
                <div class='col-xs-12 text-center'>
                    <div class='' style='background: none'>
                        <div>
                            <a href='tea-bar' class='btn btn-primary' style="background-color: #0a0a0a">
                                Hazme inmortal
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class='row' style="margin: 50px 0 50px 0">
                <div id='scrollText'>
                    <h2 data-scroll data-scroll-direction='horizontal' data-scroll-speed='-5'>En #nizamovement</h2>
                    <h2 data-scroll data-scroll-direction='horizontal' data-scroll-speed='5'>El camino del té</h2>
                    <h2 data-scroll data-scroll-direction='horizontal' data-scroll-speed='-5'>Es el desarrollo del
                        ser</h2>
                </div>
            </div>
        </div>
    </section>

    <section class='blockDorado2 blockPosition dor-bg-white' data-scroll-section>
        <div class='title-header-tab'>
            <h2>El poder del té</h2>
            <h4>Es la corriente de curación cuántica que crea un estado de absoluta plenitud</h4>
        </div>
        <img src='assets/img/temp%20video%202.jpeg' alt=''
             style='width: 100%; height: 140px; object-fit: cover;'>
        <div id='scrollText' style='margin: 50px 0 50px 0'>
            <h2 data-scroll data-scroll-direction='horizontal' data-scroll-speed='-5'
                style='margin-bottom: 0px;'>
                Cuerpo
            </h2>
            <h2 data-scroll data-scroll-direction='horizontal' data-scroll-speed='5'
                style='margin-bottom: 0px;'>
                Mente
            </h2>
            <h2 data-scroll data-scroll-direction='horizontal' data-scroll-speed='-5'>
                Alma
            </h2>
        </div>
    </section>
</section>

<!-- Comprar Te Popup -->
<section class='comprar-te' style="display: none">
    <a href='#close' onclick="localStorage.setItem('closed-comprar-te', 'true'); return false"
       class='sb-close-btn close popup-cls b-close'><i class='fa-times fa'></i></a>
    <div class='modal-content subscribe-2 blk-clr'>
        <div class='login-wrap text-center'>
            <img src='assets/img/home/NIZAPREMIUMTEALIST.jpg' alt='comprar-te'>
        </div>
    </div>
</section>
<!-- / Comprar Te Popup -->

<script>
    $(() => {
        new App.Inicio();
    })

    function openComprarTe() {
        $('.comprar-te').bPopup({
            modalClose: true, // Cierra la ventana emergente al hacer clic fuera de ella
            modalColor: 'rgba(0, 0, 0, 0.7)' // Fondo oscurecido detrás de la ventana emergente
        });
    }
</script>
