<section>
    <nav data-depth='4' class='breadcrumb hidden-sm-down dor-breadcrumb'>
        <div class='container'>
            <h1 class='category-name'>Eventos y ceremonias de té</h1>
        </div>
    </nav>
    <section class="container">
        <h2>¡LO MÁS TOP EN TU LISTA DE COSAS POR HACER!</h2>
        <h3>EL CAMINO DEL TÉ ES EL DESARROLLO DEL SER.</h3>
        <p>
            NUESTRAS CEREMONIAS Y CATAS DE TÉ SON UNA INVITACIÓN A PURIFICAR EL ALMA Y ALCANZAR UN ESTADO DE ARMONÍA CON
            NUESTRA LUZ ESPIRITUAL POR MEDIO DE UN RITUAL DE REFLEXIÓN Y AUTOCONOCIMIENTO.
        </p>
        <p>
            LAS CEREMONIAS DEL TÉ COMENZARON A PRACTICARSE HACE SIGLOS CON EL FIN DE RESETEAR NUESTRA MENTE,
            PERMITIÉNDOLE VER SIEMPRE UNA PERSPECTIVA POSITIVA, A TRAVÉS DE LA PRÁCTICA DIARIA DE LA GRATITUD, DÁNDOLE
            LA BIENVENIDA A LA ABUNDANCIA QUE EMANA DE NUESTRO ESPÍRITU.
        </p>
        <p>
            LAS CEREMONIAS DE TÉ NO SOLO SON UNA FORMA AUTÉNTICA DE SERVIR EL TÉ, SI NO UN CULTO AL ARTE.
        </p>
        <p>
            LAS CEREMONIAS SE DIVIDEN EN DISTINTAS ETAPAS.
        </p>
        <p>
            LA PRIMERA ETAPA DE LA CEREMONIA CONSISTE EN UNA MEDITACIÓN PARA QUE CONECTES CON TU SER.
        </p>
        <p>
            LA SEGUNDA ETAPA DE LA CEREMONIA CONSISTE EN BRINDARTE DICHA A TRAVÉS DE UNA EXPERIENCIA SENSORIAL.
        </p>
        <p>
            LA TERCERA ETAPA ES UNA CATA DE NUESTRA SELECCIÓN PREMIUM DE TÉ QUE BEBES EN SILENCIO APRECIANDO
            SU SABOR, DISFRUTANDO SU ESCENCIA Y ALCANZANDO LA PERFECTA ARMONÍA DE TUS SENTIDOS MIENTRAS PRACTICAS LA
            CURACIÓN CUÁNTICA.
        </p>
        <p style="text-align: center">
            ¿QUIERES HACER UNA CEREMONIA DE TÉ EN UN EVENTO PRIVADO?<br>
            <button class='btn btn-success' onclick="window.open('https://wa.me/+524776479710','_blank')">
                <i class='fab fa-whatsapp'></i>
                Whatsapp
            </button>
        </p>
    </section>
    <section class='container'>
        <div class='row'>
            <img src='assets/img/events-and-tea-ceremonies/image_6487327.JPG' alt=''>
        </div>
        <div class='row'>
            <img src='assets/img/events-and-tea-ceremonies/image_6487327%20(1).JPG' alt=''>
        </div>
    </section>
    <script>
        $(() => {
            new App.CeremoniasDeTe();
        })
    </script>
</section>
