<section>
    <nav data-depth='4' class='breadcrumb hidden-sm-down dor-breadcrumb'>
        <div class='container'>
            <h1 class='category-name'>TÉ HERBAL Y FRUTAL</h1>
        </div>
    </nav>
    <section class='container'>
        <p>
            LOS TÉS HERBALES SON TÉS SIN CAFEÍNA, SE HACEN CON MEZCLAS DE FRUTOS SECOS, PLANTAS, FLORES, SEMILLAS Y
            RAÍCES.
        </p>
        <p>
            NUESTRA SELECCIÓN PREMIUM DE TÉS INTERNACIONALES HERBALES Y FRUTALES TIENEN MÚLTIPLES BENEFICIOS PARA LA
            SALUD, INCLUYENDO UN ALTO CONTENIDO DE VITAMINAS, MINERALES Y ANTIOXIDANTES.
        </p>
        <p>
            SU CONSUMO REGULAR DESINTOXICA, QUEMA GRASA, PREVIENE EL ENVEJECIMIENTO, FORTALECIENDO LAS ENZIMAS DEL
            COLÁGENO DE NUESTRA PIEL, HIDRATA NUESTRA PIEL Y CABELLO, Y PUEDE INCLUSO AYUDAR A CURAR CIERTAS
            ENFERMEDADES, TALES COMO EL CANCER, REDUCE LOS NIVELES DE COLESTEROL MALO Y TRIGLICÉRIDOS, SON BEBIDAS
            DELICIOSAS, RELAJANTES Y REFRESCANTES, SIN AZUCAR, NI CONSERVADORES Y PUEDES SER SUPER CREATIVO CON ELLAS,
            INCLUSO PARA PREPARAR POSTRES SALUDABLES.
        </p>
    </section>
    <script>
        $(() => {
            new App.TeHerbalYFrutal();
        })
    </script>
</section>
