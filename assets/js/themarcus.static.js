Static = window.Static || {};
Static.SQUARESPACE_CONTEXT = {
    "facebookAppId": "314192535267336",
    "facebookApiVersion": "v6.0",
    "rollups": {
        "squarespace-announcement-bar": {"js": "//assets.squarespace.com/universal/scripts-compressed/announcement-bar-85a8b96795915b9e7f84d-min.en-US.js"},
        "squarespace-audio-player": {
            "css": "//assets.squarespace.com/universal/styles-compressed/audio-player-7273d8fcec67906942b35-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/audio-player-c393c9cff0f987d4dfce9-min.en-US.js"
        },
        "squarespace-blog-collection-list": {
            "css": "//assets.squarespace.com/universal/styles-compressed/blog-collection-list-3d55c64c25996c7633fc2-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-d777b4d4a1e5a0b52e24c-min.en-US.js"
        },
        "squarespace-calendar-block-renderer": {
            "css": "//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-5668de53c0ce16e20cc01-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-23dcad7ec2ed717f6fc57-min.en-US.js"
        },
        "squarespace-chartjs-helpers": {
            "css": "//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-58ae73137091cd0a61360-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-599541a905a8f30f52f33-min.en-US.js"
        },
        "squarespace-comments": {
            "css": "//assets.squarespace.com/universal/styles-compressed/comments-eeb99f32a31032af774cb-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/comments-4978efae8bb8c7e08c0b3-min.en-US.js"
        },
        "squarespace-dialog": {
            "css": "//assets.squarespace.com/universal/styles-compressed/dialog-7b3fdec47b80fd63e5e6f-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/dialog-7e20c9a3c0f9b315b9d44-min.en-US.js"
        },
        "squarespace-events-collection": {
            "css": "//assets.squarespace.com/universal/styles-compressed/events-collection-5668de53c0ce16e20cc01-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/events-collection-5b81f90d14be874ee0ec4-min.en-US.js"
        },
        "squarespace-form-rendering-utils": {"js": "//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-51880a894886ff422f192-min.en-US.js"},
        "squarespace-forms": {
            "css": "//assets.squarespace.com/universal/styles-compressed/forms-1cc007b21ede0b73086c9-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/forms-39073aad6b1ab489599f9-min.en-US.js"
        },
        "squarespace-gallery-collection-list": {
            "css": "//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-3d55c64c25996c7633fc2-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-48c9d1006aec2b6ef93c4-min.en-US.js"
        },
        "squarespace-image-zoom": {
            "css": "//assets.squarespace.com/universal/styles-compressed/image-zoom-60e14b9bac69739c96fa7-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/image-zoom-1aef7d01397062b0578b0-min.en-US.js"
        },
        "squarespace-pinterest": {
            "css": "//assets.squarespace.com/universal/styles-compressed/pinterest-3d55c64c25996c7633fc2-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/pinterest-fa27ecce216e47c4249c2-min.en-US.js"
        },
        "squarespace-popup-overlay": {
            "css": "//assets.squarespace.com/universal/styles-compressed/popup-overlay-e4ea05bd2ae9c1568e432-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/popup-overlay-ccca5a3e3a5dbf3762834-min.en-US.js"
        },
        "squarespace-product-quick-view": {
            "css": "//assets.squarespace.com/universal/styles-compressed/product-quick-view-663fb8b8c08febe7303f1-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/product-quick-view-e60b5e402a63ca2f949cb-min.en-US.js"
        },
        "squarespace-products-collection-item-v2": {
            "css": "//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-60e14b9bac69739c96fa7-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-c3055d65e385702cdc61c-min.en-US.js"
        },
        "squarespace-products-collection-list-v2": {
            "css": "//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-60e14b9bac69739c96fa7-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-e42dcafd488a1e0b855e9-min.en-US.js"
        },
        "squarespace-search-page": {
            "css": "//assets.squarespace.com/universal/styles-compressed/search-page-568ad8f2a40e76c0175c8-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/search-page-3db43a0723a12ec44227a-min.en-US.js"
        },
        "squarespace-search-preview": {"js": "//assets.squarespace.com/universal/scripts-compressed/search-preview-b94131d25eddc639a52e5-min.en-US.js"},
        "squarespace-simple-liking": {
            "css": "//assets.squarespace.com/universal/styles-compressed/simple-liking-47606e375db2b296c3464-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/simple-liking-1159db08b6e09ca663491-min.en-US.js"
        },
        "squarespace-social-buttons": {
            "css": "//assets.squarespace.com/universal/styles-compressed/social-buttons-b186d09e02921fd7f8e00-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/social-buttons-763529a4a7472185e446d-min.en-US.js"
        },
        "squarespace-tourdates": {
            "css": "//assets.squarespace.com/universal/styles-compressed/tourdates-3d55c64c25996c7633fc2-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/tourdates-7af6e456d899245a0204d-min.en-US.js"
        },
        "squarespace-website-overlays-manager": {
            "css": "//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-5c2f030f6ee94f066dc3d-min.en-US.css",
            "js": "//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-f3b999a82ae9297f7cd83-min.en-US.js"
        }
    },
    "pageType": 2,
    "website": {
        "id": "5e8f5d9bae8f137fbc83492a",
        "identifier": "porpoise-hexahedron-lys7",
        "websiteType": 1,
        "contentModifiedOn": 1643836851566,
        "cloneable": false,
        "hasBeenCloneable": false,
        "developerMode": true,
        "siteStatus": {},
        "language": "en-US",
        "timeZone": "America/New_York",
        "machineTimeZoneOffset": -18000000,
        "timeZoneOffset": -18000000,
        "timeZoneAbbr": "EST",
        "siteTitle": "Marcus Eriksson",
        "fullSiteTitle": "Marcus Eriksson",
        "siteDescription": "",
        "socialLogoImageId": "60414eb4c13a867fd2577d8e",
        "shareButtonOptions": {"3": true, "1": true, "2": true, "4": true, "8": true, "6": true, "7": true},
        "socialLogoImageUrl": "//images.squarespace-cdn.com/content/v1/5e8f5d9bae8f137fbc83492a/1614892725051-BK1H4MXDNUJ306VVIET1/share.png",
        "authenticUrl": "https://themarcus.com",
        "internalUrl": "https://porpoise-hexahedron-lys7.squarespace.com",
        "baseUrl": "https://themarcus.com",
        "primaryDomain": "themarcus.com",
        "sslSetting": 3,
        "isHstsEnabled": false,
        "typekitId": "",
        "statsMigrated": false,
        "imageMetadataProcessingEnabled": false,
        "screenshotId": "d930f607d83f33cdba330f53d083beb03de724f0af40c6128056426c5c81a10d",
        "captchaSettings": {"enabledForDonations": false},
        "showOwnerLogin": false
    },
    "websiteSettings": {
        "id": "5e8f5d9bae8f137fbc83492d",
        "websiteId": "5e8f5d9bae8f137fbc83492a",
        "subjects": [],
        "country": "US",
        "state": "NJ",
        "simpleLikingEnabled": true,
        "mobileInfoBarSettings": {
            "isContactEmailEnabled": false,
            "isContactPhoneNumberEnabled": false,
            "isLocationEnabled": false,
            "isBusinessHoursEnabled": false
        },
        "commentLikesAllowed": true,
        "commentAnonAllowed": true,
        "commentThreaded": true,
        "commentApprovalRequired": false,
        "commentAvatarsOn": true,
        "commentSortType": 2,
        "commentFlagThreshold": 0,
        "commentFlagsAllowed": true,
        "commentEnableByDefault": true,
        "commentDisableAfterDaysDefault": 0,
        "disqusShortname": "",
        "commentsEnabled": false,
        "storeSettings": {
            "returnPolicy": null,
            "termsOfService": null,
            "privacyPolicy": null,
            "expressCheckout": false,
            "continueShoppingLinkUrl": "/",
            "useLightCart": false,
            "showNoteField": false,
            "shippingCountryDefaultValue": "US",
            "billToShippingDefaultValue": false,
            "showShippingPhoneNumber": true,
            "isShippingPhoneRequired": false,
            "showBillingPhoneNumber": true,
            "isBillingPhoneRequired": false,
            "currenciesSupported": ["USD", "CAD", "GBP", "AUD", "EUR", "CHF", "NOK", "SEK", "DKK", "NZD", "SGD", "MXN", "HKD", "CZK", "ILS", "MYR", "RUB", "PHP", "PLN", "THB", "BRL", "ARS", "COP", "IDR", "INR", "JPY", "ZAR"],
            "defaultCurrency": "USD",
            "selectedCurrency": "USD",
            "measurementStandard": 1,
            "showCustomCheckoutForm": false,
            "checkoutPageMarketingOptInEnabled": false,
            "enableMailingListOptInByDefault": false,
            "sameAsRetailLocation": false,
            "merchandisingSettings": {
                "scarcityEnabledOnProductItems": false,
                "scarcityEnabledOnProductBlocks": false,
                "scarcityMessageType": "DEFAULT_SCARCITY_MESSAGE",
                "scarcityThreshold": 10,
                "multipleQuantityAllowedForServices": true,
                "restockNotificationsEnabled": false,
                "restockNotificationsMailingListSignUpEnabled": false,
                "relatedProductsEnabled": false,
                "relatedProductsOrdering": "random",
                "soldOutVariantsDropdownDisabled": false,
                "productComposerOptedIn": false,
                "productComposerABTestOptedOut": false,
                "productReviewsEnabled": false
            },
            "isLive": false,
            "multipleQuantityAllowedForServices": true
        },
        "useEscapeKeyToLogin": true,
        "ssBadgeType": 1,
        "ssBadgePosition": 4,
        "ssBadgeVisibility": 1,
        "ssBadgeDevices": 1,
        "pinterestOverlayOptions": {"mode": "disabled"},
        "ampEnabled": false
    },
    "cookieSettings": {
        "isCookieBannerEnabled": false,
        "isRestrictiveCookiePolicyEnabled": false,
        "isRestrictiveCookiePolicyAbsolute": false,
        "cookieBannerText": "",
        "cookieBannerTheme": "",
        "cookieBannerVariant": "",
        "cookieBannerPosition": "",
        "cookieBannerCtaVariant": "",
        "cookieBannerCtaText": "",
        "cookieBannerAcceptType": "OPT_IN",
        "cookieBannerOptOutCtaText": ""
    },
    "websiteCloneable": false,
    "collection": {"title": "Home", "id": "5e8f5ecd4882d92a9da72f97", "fullUrl": "/", "type": 10, "permissionType": 1},
    "subscribed": false,
    "appDomain": "squarespace.com",
    "templateTweakable": true,
    "tweakJSON": {
        "maxPageWidth": "1200px",
        "pagePadding": "3vw",
        "product-gallery-auto-crop": "false",
        "product-image-auto-crop": "true",
        "tweak-portfolio-slides-cover-overlay-color": "#38383B",
        "tweak-portfolio-slides-inset-overlay-color": "#38383B",
        "tweak-portfolio-slides-split-overlay-color": "#38383B",
        "tweak-v1-related-products-title-spacing": "50px"
    },
    "templateId": "5e8f5dc001e1e4727920d9c5",
    "templateVersion": "7.1",
    "pageFeatures": [1, 2, 4],
    "gmRenderKey": "QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4",
    "templateScriptsRootUrl": "https://static1.squarespace.com/static/ta/5e8f5d9bae8f137fbc83492a/2012/scripts/",
    "betaFeatureFlags": ["commerce_clearpay", "commerce_onboarding_tools_screen_test", "commerce_activation_experiment_add_payment_processor_card", "member_areas_spanish_interviews", "commerce_site_visitor_metrics", "campaigns_blog_product_image_editor", "google_analytics_4_gtag_js", "commerce_product_composer_ab_test_all_users", "commerce_category_id_discounts_enabled", "category-delete-product-service-enabled", "themes", "campaigns_audience_card", "commerce_etsy_shipping_import", "campaigns_hide_deleted_automations_panel", "reduce_general_search_api_traffic", "background_art_onboarding", "nested_categories", "COMMERCE_PRODUCT_REVIEWS_MERCHANT_EMAIL", "commerce_restock_notifications", "campaigns_thumbnail_layout", "commerce_afterpay_pdp", "customer_account_creation_recaptcha", "site_header_footer", "section_admin", "campaigns_banner_reduce_size", "commerce_fts_review_survey", "campaigns_new_subscriber_search", "override_block_styles", "campaigns_alt_text", "collection_typename_switching", "startup_checklist", "campaigns_content_editing_survey", "viewer-role-contributor-invites", "campaigns_show_apply_website_styles_button", "commerce_etsy_product_import", "new_stacked_index", "commerce_pdp_layouts_ga", "campaigns_global_uc_ab", "product-retriever-v2-enabled", "member_areas_schedule_interview", "campaigns_section_reorder_arrows", "scripts_defer", "commerce_product_branching", "nested_categories_migration_enabled"],
    "impersonatedSession": false,
    "tzData": {
        "zones": [[-300, "US", "E%sT", null]],
        "rules": {"US": [[1967, 2006, null, "Oct", "lastSun", "2:00", "0", "S"], [1987, 2006, null, "Apr", "Sun>=1", "2:00", "1:00", "D"], [2007, "max", null, "Mar", "Sun>=8", "2:00", "1:00", "D"], [2007, "max", null, "Nov", "Sun>=1", "2:00", "0", "S"]]}
    },
    "showAnnouncementBar": false
};
