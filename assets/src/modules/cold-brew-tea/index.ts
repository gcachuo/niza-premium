import {tns} from 'tiny-slider';

export class ColdBrewTea {
    constructor() {
        $.each($(".slider"), (i, e) => {
            tns({
                container: e,
                items: 4,
                slideBy: 'page',
                autoplay: true
            });
        });
    }
}
